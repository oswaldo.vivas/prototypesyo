/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.*;

/**
 *
 * @author win10
 */
public class Controller implements ActionListener{
    private final principalView princView;
    private final localesView locView;
    private final extranjerasView extView;

    public Controller(principalView princView, localesView locView, extranjerasView extView){
        this.princView=princView;
        this.locView=locView;
        this.extView=extView;
        this.princView.add(princView.jLayeredPane2);
        this.princView.buttonLogin.addActionListener(this);
        this.princView.buttonRegistrer.addActionListener(this);
        this.princView.buttonServices.addActionListener(this);
        this.princView.jButton3.addActionListener(this);
        this.princView.jButton4.addActionListener(this);
        this.princView.jButton5.addActionListener(this);
        this.princView.jButton2.addActionListener(this);
        this.locView.jButton5.addActionListener(this);
        this.locView.jButton7.addActionListener(this);
        this.extView.jButton5.addActionListener(this);
        this.extView.jButton7.addActionListener(this);
    }
    public void iniciar(){
        princView.setTitle("Proyecto Syo");
        princView.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource()==extView.jButton5){
            extView.jPanel5.setVisible(true);
            extView.jPanel4.setVisible(false);
        }
        if (e.getSource()==extView.jButton7){
            extView.jPanel5.setVisible(false);
            extView.jPanel4.setVisible(false);
            extView.jPanel16.setVisible(true);
        }
        if (e.getSource()==locView.jButton5){
            locView.jPanel5.setVisible(true);
            locView.jPanel4.setVisible(false);
        }
        if (e.getSource()==locView.jButton7){
            locView.jPanel5.setVisible(false);
            locView.jPanel4.setVisible(false);
            locView.jPanel16.setVisible(true);
        }
        if (e.getSource()==princView.jButton2){
            princView.setVisible(false);
            //locView.setVisible(true);
            extView.setVisible(true);
        }
        if (e.getSource()==princView.jButton3||e.getSource()==princView.jButton4||e.getSource()==princView.jButton5){
            princView.principalPanel.setVisible(true);
            princView.loginPanel.setVisible(false);
            princView.registrerPanel.setVisible(false);
            princView.servicesPanel.setVisible(false);
        }
        if (e.getSource()==princView.buttonLogin){
            princView.loginPanel.setVisible(true);
            princView.principalPanel.setVisible(false);
            princView.registrerPanel.setVisible(false);
            princView.servicesPanel.setVisible(false);
        }
        if (e.getSource()==princView.buttonRegistrer){
            princView.registrerPanel.setVisible(true);
            princView.principalPanel.setVisible(false);
            princView.loginPanel.setVisible(false);
            princView.servicesPanel.setVisible(false);
        }
        if (e.getSource()==princView.buttonServices){
            princView.servicesPanel.setVisible(true);
            princView.principalPanel.setVisible(false);
            princView.loginPanel.setVisible(false);
            princView.registrerPanel.setVisible(false);
        }
        
    }
    
}
